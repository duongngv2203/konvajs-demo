
import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss']
})
export class LineComponent {
  public height = window.innerHeight;
  public width = window.innerWidth;
  ngOnInit() {
    const stage = new Konva.Stage({
      container: 'container',
      width: this.width,
      height: this.height
    })

    const layer = new Konva.Layer;

    const redLine = new Konva.Line({
      points: [5, 10, 70, 60, 160, 40, 300, 60],
      stroke: 'red',
      strokeWidth: 2,
      lineCap: 'round',
      lineJoin: 'round',
      tension: 0.6,      
    });

    const polyBlue = new Konva.Line({
      points: [5, 60, 65, 60, 65, 5, 5, 5],
      stroke: 'blue',
      strokeWidth: 2,
      lineJoin: 'round',
      // dash: [10, 10],
      fill: 'rgba(135, 206, 250, 0.6)',
      closed: true
    })

    const greenLine = new Konva.Line({
      points: [23, 20, 23, 160, 70, 93, 150, 109, 290, 139, 270, 93],
      stroke: 'green',
      strokeWidth: 2,
      lineCap: 'round',
      lineJoin: 'round',
      dash: [20, 5, 2, 5],
      closed: true,
      fill: '#87CEFA',
      tension: 0.7,
    })

    redLine.move({
      x: 100,
      y: 500,
    });
    greenLine.move({
      x: 40,
      y: 55,
    });
    polyBlue.move({
      x: 100,
      y: 200,
    }); 

    layer.add(redLine);
    layer.add(polyBlue);
    layer.add(greenLine);

    stage.add(layer);
  }
}
