import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-sprite',
  templateUrl: './sprite.component.html',
  styleUrls: ['./sprite.component.scss']
})
export class SpriteComponent {
  public width = window.innerWidth;
  public height = window.innerHeight;

  ngOnInit() {
    const stage = new Konva.Stage({
      container: 'container',
      height: this.height,
      width: this.width,
    });

    const layer = new Konva.Layer;

    const animations = {
      idle: [
        // x, y, width, height (4 frames)
        2, 2, 70, 119, 71, 2, 74, 119, 146, 2, 81, 119, 226, 2, 76, 119,],
      punch: [
        // x, y, width, height (4 frames)
        2, 138, 74, 122, 76, 138, 84, 122, 346, 138, 120, 122,],
    };
    
    const imgObj = new Image;
    imgObj.src = '/assets/sprite.png';
    // const blod = new Konva.Sprite({
      
      // })
      
    const blob = new Konva.Sprite({
      image: imgObj,
      animation: 'idle',
      animations: animations,
    })
    
    imgObj.onload = () => {
      blob.setAttrs({
        x: 150,
        y: 150,
        frameRate: 10,
        frameIndex: 0,
      });
    }
      
    layer.add(blob);
    
    stage.add(layer);
    
    blob.start();
    
    document.getElementById('punch')!.addEventListener('click', () => {
      blob.animation('punch');
      blob.on('frameIndexChange.button', () => {
        if (blob.frameIndex() === 2) {
          setTimeout(() => {
            blob.animation('idle');
            blob.off('.button');
          }, 1000 / blob.frameRate());
        }
      });
    }, false);
  };
}
