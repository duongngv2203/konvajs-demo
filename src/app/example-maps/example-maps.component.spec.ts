import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleMapsComponent } from './example-maps.component';

describe('ExampleMapsComponent', () => {
  let component: ExampleMapsComponent;
  let fixture: ComponentFixture<ExampleMapsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExampleMapsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExampleMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
