import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-example-maps',
  templateUrl: './example-maps.component.html',
  styleUrls: ['./example-maps.component.scss']
})
export class ExampleMapsComponent {
  public stage = new Konva.Stage({
    container: 'container',
    height: window.innerHeight,
    width: window.innerWidth
  })
  ngOnInit() {
    
  }
}
