import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss']
})
export class CircleComponent {
  public setCircle() {
    var stage = new Konva.Stage ({
      height: window.innerHeight,
      width: window.innerWidth,
      container: 'container'
    });

    var layer = new Konva.Layer;

    var circle = new Konva.Circle ({
      x: stage.width() / 2,
      y: stage.height() / 2,
      radius: 50,
      fill: 'blue',
      stroke: 'black',
      strokeWidth: 1
    })

    layer.add(circle);

    stage.add(layer);
  } 

  ngOnInit() {
    this.setCircle()
  }
}
