import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-single-text-box',
  templateUrl: './single-text-box.component.html',
  styleUrls: ['./single-text-box.component.scss']
})
export class SingleTextBoxComponent {
  public height = window.innerHeight;
  public width = window.innerWidth;

  ngOnInit() {
    const stage = new Konva.Stage({
      container: 'container',
      height: this.height,
      width: this.width,
    });

    const layer = new Konva.Layer;

    const simpleText = new Konva.Text({
      x: stage.width() / 2,
      y: 50,
      text: 'A Simple Text',
      fontSize: 45,
      fontFamily: 'Roboto',
      fill: 'green',
    });

    simpleText.offsetX(simpleText.width());

    const complexText = new Konva.Text({
      x: stage.width() / 2,
      y: 100,
      text: "COMPLEX TEXT\n\nAll the world's a stage, and all the men and women merely players. They have their exits and their entrances.",
      fontSize: 18,
      fontFamily: 'Roboto',
      fill: '#555',
      width: 300,
      padding: 20,
      align: 'center',
    });

    const rect = new Konva.Rect({
      x: stage.width() / 2,
      y: 100,
      stroke: '#555',
      strokeWidth: 5,
      fill: '#ddd',
      width: 300,
      height: complexText.height(),
      shadowColor: 'black',
      shadowBlur: 10,
      shadowOffsetX: 10,
      shadowOffsetY: 10,
      shadowOpacity: 0.2,
      cornerRadius: 10,
    });

    layer.add(simpleText);
    layer.add(rect);
    layer.add(complexText);

    stage.add(layer);
  }
}
