import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleTextBoxComponent } from './single-text-box.component';

describe('SingleTextBoxComponent', () => {
  let component: SingleTextBoxComponent;
  let fixture: ComponentFixture<SingleTextBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleTextBoxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SingleTextBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
