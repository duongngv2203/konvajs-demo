import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KonvaImageComponent } from './konva-image.component';

describe('KonvaImageComponent', () => {
  let component: KonvaImageComponent;
  let fixture: ComponentFixture<KonvaImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KonvaImageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KonvaImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
