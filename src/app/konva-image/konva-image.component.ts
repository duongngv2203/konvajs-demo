import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-konva-image',
  templateUrl: './konva-image.component.html',
  styleUrls: ['./konva-image.component.scss']
})
export class ImageComponent {
  // public height = window.innerHeight;
  // public width = window.innerWidth;

  // ngOnInit() {
  //   const stage = new Konva.Stage({
  //     height: this.height,
  //     width: this.width,
  //     container: 'container',
  //   });

  //   const layer = new Konva.Layer();

  //   const imgObj = new Image;
  //   imgObj.onload = () => {
  //     const yoda = new Konva.Image({
  //       x: 0,
  //       y: 0,
  //       image: imgObj,
  //       width: 106,
  //       height: 118,
  //     });
  //     layer.add(yoda);
  //   }

  //   imgObj.src = '/assets/yoda.png';

  //   Konva.Image.fromURL('/assets/darth-vader.png', (vader) => {
  //     vader.setAttrs({
  //       x: 200,
  //       y: 50,
  //       scaleX: 0.5,
  //       scaleY: 0.5,
  //       cornerRadius: 20,
  //     });
  //     layer.add(vader);
  //   })

  //   stage.add(layer);
  // }
}
