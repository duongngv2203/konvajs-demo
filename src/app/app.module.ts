import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CircleComponent } from './circle/circle.component';
import { WedgeComponent } from './wedge/wedge.component';
import { ElipseComponent } from './elipse/elipse.component';
import { ExampleMapsComponent } from './example-maps/example-maps.component';
import { LineComponent } from './line/line.component';
import { SpriteComponent } from './sprite/sprite.component';
import { ImageComponent } from './konva-image/konva-image.component';
import { SingleTextBoxComponent } from './single-text-box/single-text-box.component';
import { HomeComponent } from './home/home.component'
@NgModule({
  declarations: [
    AppComponent,
    CircleComponent,
    WedgeComponent,
    ElipseComponent,
    ExampleMapsComponent,
    LineComponent,
    SpriteComponent,
    ImageComponent,
    SingleTextBoxComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
