import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WedgeComponent } from './wedge.component';

describe('WedgeComponent', () => {
  let component: WedgeComponent;
  let fixture: ComponentFixture<WedgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WedgeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WedgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
