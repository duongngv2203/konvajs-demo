import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-wedge',
  templateUrl: './wedge.component.html',
  styleUrls: ['./wedge.component.scss']
})
export class WedgeComponent {
  public setCircle() {
    var stage = new Konva.Stage ({
      height: window.innerHeight,
      width: window.innerWidth,
      container: 'container'
    });

    var layer = new Konva.Layer;

    var wedge = new Konva.Wedge({
      x: stage.width() / 2,
      y: stage.height() / 2,
      radius: 70,
      angle: 90,
      fill: 'red',
      stroke: 'black',
      strokeWidth: 4,
      rotation: 135,
    });

    layer.add(wedge);

    stage.add(layer);
  } 

  ngOnInit() {
    this.setCircle()
  }

}
