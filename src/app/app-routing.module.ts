import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CircleComponent } from './circle/circle.component';
import { LineComponent } from './line/line.component';
import { SingleTextBoxComponent } from './single-text-box/single-text-box.component';
import { WedgeComponent } from './wedge/wedge.component';
import { ImageComponent } from './konva-image/konva-image.component';
import { ElipseComponent } from './elipse/elipse.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'circle', component: CircleComponent},
  { path: 'line', component: LineComponent },
  { path: 'single-text-box', component: SingleTextBoxComponent },
  { path: 'wedge', component: WedgeComponent },
  { path: 'konva-image', component: ImageComponent },
  { path: 'elipse', component: ElipseComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
