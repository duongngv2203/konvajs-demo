import { Component } from '@angular/core';
import Konva from 'konva';

@Component({
  selector: 'app-elipse',
  templateUrl: './elipse.component.html',
  styleUrls: ['./elipse.component.scss']
})
export class ElipseComponent {
  ngOnInit() {
    var stage = new Konva.Stage({
      container: 'container',
      height: window.innerHeight,
      width: window.innerWidth
    })

    var layer = new Konva.Layer;

    var oval = new Konva.Ellipse({
      x: stage.width() / 2,
      y: stage.height() / 2,
      radiusX: 50,
      radiusY: 70,
      fill: 'blue',
      stroke: 'white',
      strokeWidth: 0.1
    })

    layer.add(oval);

    stage.add(layer);
  }
}
