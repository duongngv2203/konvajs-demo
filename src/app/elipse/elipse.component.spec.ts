import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElipseComponent } from './elipse.component';

describe('ElipseComponent', () => {
  let component: ElipseComponent;
  let fixture: ComponentFixture<ElipseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElipseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ElipseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
